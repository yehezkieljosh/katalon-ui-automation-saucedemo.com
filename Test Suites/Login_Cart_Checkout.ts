<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login_Cart_Checkout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c98dd017-ba90-417e-8a73-22845ee9aaf3</testSuiteGuid>
   <testCaseLink>
      <guid>e2e7f1d5-c9a9-480f-b52c-fe8060049cf2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbeb5622-92ea-45a4-bff0-54ec816c93ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Add_to_cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff48fd36-0700-465b-848d-67bfaf7ba277</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout_Page/Checkout_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
