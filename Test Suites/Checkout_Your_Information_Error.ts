<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Checkout_Your_Information_Error</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0a95a66c-5b28-44e2-bd80-dc4c2ae204b8</testSuiteGuid>
   <testCaseLink>
      <guid>ac256dfb-c902-4759-b29d-5e0a3fa568d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56421c5c-508f-4d58-b89b-ba767de7f401</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Add_to_cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33da172d-20e8-4454-a351-b1f8d1109b7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout_Page/Your_Information_Page/First_Name_Error</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89ab6c61-46aa-4933-ba08-4f996407f1ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout_Page/Your_Information_Page/Last_Name_Error</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a6f52b4-a34c-4233-a5f2-7edc79ac9091</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout_Page/Your_Information_Page/Postal_Code_Error</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
